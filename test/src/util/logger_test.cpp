
/**
 * 测试util中的logger类
 * logger类用于记录日志，并且提供日志分级
 * logger也可以同时输出到文件和终端
 * logger类按照配置的文件大小分割日志文件，同时也支持按天分割文件
 * @Author: Allen.Wu(allenlikeu@gmail.com)
 * @Date: 2015.05.24
 */

#include <gtest/gtest.h>
#include <util/logger.h>

//TODO(allenlikeu@gmail.com):测试用例未考虑清楚，2015.05.28前天内补全

